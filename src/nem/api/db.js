const mongoose = require('mongoose');
mongoose.connect(process.env.MONGODB_CONNSTRING||'mongodb://lemonde:denemo@mgdb-ctnr', {
                            useNewUrlParser: true,
                            useUnifiedTopology: true,
                            useFindAndModify: false,
                            useCreateIndex: true
                          });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log("MongoDB Connected...");
});

module.exports = db
